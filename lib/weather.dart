import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {

      return MaterialApp(
        home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: buildAppBarWidget(),
          body: buildBodyWidget(),
          bottomNavigationBar: buildBottomNavigationBarWidget(),
        ),
      );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Color.fromARGB(0, 224, 222, 222),
    elevation: 0,
    leading: IconButton(
      icon: Icon(
        Icons.search,
        size: 30,
        color: Colors.black,
      ),
      color: Colors.white,
      onPressed: () {},
    ),
    actions: [
      IconButton(
        icon: Icon(
          Icons.menu,
          size: 30,
          color: Colors.black,
        ),
        color: Colors.white,
        onPressed: () {},
      ),
    ],
  );
}

BottomNavigationBar buildBottomNavigationBarWidget() {
  return BottomNavigationBar(
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.map,
          size: 20,
        ),
        label: '',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.list, size: 20),
        label: '',
      ),
    ],
    currentIndex: 0,
    selectedItemColor: Colors.amber[800],
  );
}

buildlocationWidget() {
  return Column(
    children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Bangkok',
            style: TextStyle(
                fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ],
      ),
    ],
  );
}

buildtempurateWidget() {
  return Column(children: [
    Text(
      '32°C',
      style: TextStyle(
          fontSize: 35, fontWeight: FontWeight.bold, color: Colors.white),
    ),
    Icon(
      Icons.sunny,
      size: 50,
      color: Colors.white,
    ),
  ]);
}

buildtimeWidget() {
  return Column(
    children: [
      Text(
        'Today',
        style: TextStyle(
            fontSize: 35, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '12:00',
        style: TextStyle(
            fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),
      ),
    ],
  );
}

buildcityWidget(String city, String temp, IconData icon) {
  return Column(
    children: [
      Text(
        city,
        style: TextStyle(
            fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        temp,
        style: TextStyle(
            fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Icon(
        icon,
        size: 30,
      ),
    ],
  );
}

Widget buildWeatherWidget() {
  return Container(
    margin: EdgeInsets.all(5),
    decoration: ShapeDecoration(
      color: Colors.black12,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15)
      ),
    ),
    height: 500,

  );
}
BuildDaywidget(String day, String temp, IconData icon){
  return Column(
    children: [
      Text(
        day,
        style: TextStyle(
            fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        temp,
        style: TextStyle(
            fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Icon(icon, size: 30, color: Colors.white),
    ],
  );
}


Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.cyan,
          image: DecorationImage(
            image: NetworkImage(
                "https://i.pinimg.com/originals/7a/35/d8/7a35d83ba22e7c61b3e19a76057657ed.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  buildlocationWidget(),
                  buildtempurateWidget(),
                  buildtimeWidget(),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      Row(

                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          BuildDaywidget('Mon', '32°C', Icons.sunny),
                          BuildDaywidget('Tue', '32°C', Icons.sunny),
                          BuildDaywidget('Wed', '32°C', Icons.sunny),
                          BuildDaywidget('Thu', '32°C', Icons.sunny),
                          BuildDaywidget('Fri', '32°C', Icons.sunny),
                          BuildDaywidget('Sat', '32°C', Icons.sunny),
                          BuildDaywidget('Sun', '32°C', Icons.sunny),
                        ],
                      ),
                    ],
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('New York'),
                    subtitle: Text('USA'),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('Tokyo'),
                    subtitle: Text('Japan'),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('Paris'),
                    subtitle: Text('France'),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('Rome'),
                    subtitle: Text('Italy'),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('BeiJing'),
                    subtitle: Text('China'),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
